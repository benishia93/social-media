const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = (req, res, next) => {
  const token = req.header("authentication-token");
  const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
  req.userId = decoded.userId;
  //return decoded.userId;
  next();
  //console.log(decoded.userId);
};
