const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let postSchema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    name: {
      type: String,
      required: true,
    },
    date: {
      type: Date,
      default: Date.now(),
    },
    text: {
      type: String,
      required: true,
    },
    likes: [
      {
        user: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "user",
        },
      },
    ],
  },
  {
    timestamps: true,
    collection: "posts",
  }
);
module.exports = mongoose.model("Post", postSchema);
