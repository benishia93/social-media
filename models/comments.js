const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let commentsSchema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    post: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "post",
    },
    date: {
      type: Date,
      default: Date.now(),
    },
    comment: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
    collection: "comments",
  }
);
module.exports = mongoose.model("Comment", commentsSchema);
