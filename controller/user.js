const User = require("../models/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

exports.getUser = async (req, res) => {
  try {
    let users = await User.find().select("-password");
    res.json(users);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send("server error");
  }
};

exports.getUserByEmail = async (req, res) => {
  try {
    let userEmail = req.params.user_email;
    let user = await User.findOne({ email: userEmail }).select("-password");
    res.json(user);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send("server error");
  }
};

exports.getUserById = async (req, res) => {
  try {
    let userId = req.params.user_id;
    let user = await User.findById(userId).select("-password");
    res.json(user);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send("server error");
  }
};
