const User = require("../models/User");
const Post = require("../models/post");
const Comment = require("../models/comments");

exports.addComment = async (req, res) => {
  try {
    let post = await Post.findById(req.params.post_id);
    let user = await User.findById(req.userId).select("-password");

    const { comment } = req.body;
    let errors = [];
    if (!comment) {
      error.push("text is required");
    }
    if (errors.length > 0) {
      return res.status(422).json({ errors: errors });
    }
    if (!user) return res.status(404).json("User not found");

    if (!post) return res.status(404).json("Post not found");

    let newComment = new Comment({
      comment,
      user: req.userId,
      post: req.params.post_id,
    });

    await newComment.save();

    res.json("Comment is added");
  } catch (error) {
    console.error(error);
    return res.status(500).json("Server Error...");
  }
};
