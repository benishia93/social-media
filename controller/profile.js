const User = require("../models/User");

exports.getProfile = async (req, res) => {
  try {
    let { userName } = req.body;
    let errors = [];
    if (!userName) {
      errors.push("required username");
    }
    if (errors.length > 0) {
      return res.status(422).json({ errors: errors });
    }

    let users = await User.find().select("-password");

    let findUserByUsername = users.filter(
      (user) =>
        user.name.toString().toLowerCase().split(" ").join("") ===
        userName.toString().toLowerCase().split(" ").join("")
    );
    res.json(findUserByUsername);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send("Server error.");
  }
};

exports.updateProfile = async (req, res) => {
  try {
    const { changeUserData } = req.body;
    let user = await User.findById(req.userId).select("-password");
    //console.log(changeUserData);
    let errors = [];
    if (!changeUserData) {
      errors.push({ changeUserData: "required" });
    }
    if (errors.length > 0) {
      return res.status(422).json({ errors: errors });
    }

    if (!user) return res.status(404).json("User not found");

    //userDataToChange -> name,lastName,userName

    let userDataToChange = req.params.user_data_to_change.toString();

    if (user[userDataToChange] === changeUserData.toString())
      return res
        .status(401)
        .json("This is the same data that is already in database");

    user[userDataToChange] = changeUserData.toString();

    await user.save();

    res.json("Data is changed");
  } catch (error) {
    console.error(error);
    return res.status(500).json("Server Error...");
  }
};
