const User = require("../models/User");
const Post = require("../models/post");

exports.createPost = async (req, res) => {
  let { text } = req.body;
  let errors = [];
  if (!text) {
    error.push("text is required");
  }
  if (errors.length > 0) {
    return res.status(422).json({ errors: errors });
  }
  try {
    let user = await User.findById(req.userId).select("-password");
    if (!user) return res.status(404).json("User not found");

    let newPost = new Post({
      text,
      name: user.name,
      user: req.userId,
    });

    await newPost.save();

    res.json("Post is created, congratulations!");
  } catch (error) {
    console.error(error);
    return res.status(500).json("Server Error...");
  }
};

exports.addLike = async (req, res) => {
  try {
    let post = await Post.findById(req.params.post_id);

    if (!post) return res.status(404).json("Post not found");

    if (post.likes.find((like) => like.user.toString() === req.userId))
      return res.status(401).json("Post is already liked by you!");

    let newLike = {
      user: req.userId,
    };

    post.likes.unshift(newLike);

    await post.save();

    res.json(post);
  } catch (error) {
    console.error(error);
    return res.status(500).json("Server Error...");
  }
};
