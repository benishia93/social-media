const express = require("express");
const router = express.Router();

const authentication = require("../Middleware/authentication");

const { getUser, getUserByEmail, getUserById } = require("../controller/user");

const { getProfile, updateProfile } = require("../controller/profile");
//const updateProfile = require("../controller/updateProfile");

router.get("/get_user_by_id/:user_id", getUserById);
router.get("/users", getUser);
router.get("/get_user_by_email/:user_email", getUserByEmail);
router.get("/get_profile", getProfile);
router.put(
  "/updateProfile/:user_data_to_change",
  authentication,
  updateProfile
);

module.exports = router;
