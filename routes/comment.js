const express = require("express");
const router = express.Router();

const authentication = require("../Middleware/authentication");
const { addComment } = require("../controller/comment");

router.put("/add_comment/:post_id", authentication, addComment);

module.exports = router;
