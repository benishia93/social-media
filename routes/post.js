const express = require("express");
const router = express.Router();

const authentication = require("../Middleware/authentication");
const { createPost, addLike } = require("../controller/post");

router.post("/", authentication, createPost);
router.put("/likes/:post_id", authentication, addLike);

module.exports = router;
